/**
 * Created by phucnguyen on 29/09/2016.
 */
var log4js = require("log4js");
var date = new Date();
log4js.configure({
    appenders: [
        {
            type: "file",
            filename: "server" + "." + date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + ".log",
            "maxLogSize": 1056784,
            "level": "INFO"
        },
        {
            type: "console"
        }
    ],
    replaceConsole: true
});

var getLogger = function (file) {
    return log4js.getLogger(file);
}

module.exports = {
    getLogger: getLogger
}