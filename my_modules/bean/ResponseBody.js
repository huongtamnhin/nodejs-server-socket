/**
 * Created by phuchoangmai on 30/09/2016.
 */
var simpleResponse = function(status, onlyMessage){
    var response = {
        'status': status,
        'onlyMessage': onlyMessage
    }
    return response;
}

var mediumResponse = function(status, onlyMessage, code,data){
    var response = {
        'status': status,
        'onlyMessage': onlyMessage,
        'code': code,
        'data': data
    }
    return response;
}

var gameConfigResponse = function(status, onlyMessage, code, gameId, data){
    var response = {
        'status': status,
        'onlyMessage': onlyMessage,
        'code': code,
        'gameId': gameId,
        'data': data
    }
    return response;
}

module.exports = {
    simpleResponse: simpleResponse,
    mediumResponse: mediumResponse,
    gameConfigResponse: gameConfigResponse
}