var app = require('express')(); //Framework of nodejs
var http = require('http').Server(app);
var io = require('socket.io')(http); //Module socket.io
var config = require("./my_modules/config/ConfigLog4JS"); //module Log4JS
var logger = config.getLogger("app.js"); //Get logger for this file
var gameLogic = require("./my_modules/main/GameLogic"); //module Log4JS
var responseBody = require("./my_modules/bean/ResponseBody");
var constantVariable = require("./my_modules/sharevariables/ConstantVariable"); //module constant variable
var questionGame = require("./my_modules/main/QuestionGame");

var ROOM_DEFAULT = 'ROOM_DEFAULT'; //Default room

var listUser = {};

app.get('/getListUsers', function (req, res) {
    var requestBody = req.body?req.body:{};
    if(!requestBody['userId']){
        responseBody.mediumResponse('F','Bad request', constantVariable.BAD_REQUEST, null);
        return;
    }

    logger.info("["+requestBody.userId+"] request getListUers");
    res.json(responseBody.mediumResponse('T','Get list users success',
        constantVariable.LIST_USERS, listUser));
});

// express route to ping server.
app.get('/ping', function(req, res) {
    logger.info("A client is pinging to server");
    res.json(responseBody.simpleResponse('T','pong'));
});

app.post('/login', function(req, res) {
    logger.info("Login from: " + req.headers['user-agent']);
    var request = req.body?req.body:{};
    if(request['userId']){
        listUser[request.userId] = 'T';
        res.json(responseBody.simpleResponse('T','Login success'));
    }else{
        responseBody.mediumResponse('F','Bad request', constantVariable.BAD_REQUEST, null);
    }
});

io.on('connection', function (socket) {
    logger.info('Total player is online: '+Object.keys(listUser).length);

    io.emit(constantVariable.ALL_MESSAGE,
        responseBody.mediumResponse('T','Total player is online: '+Object.keys(listUser).length,
        constantVariable.NEW_MESSAGE,''));

    socket.emit(constantVariable.ALL_MESSAGE,
        responseBody.mediumResponse('T','Welcome to this game',
            constantVariable.CONFIG_SOCKET,{
                'mySocketId': socket.id
            }));

    gameLogic.initGame(io, socket);
});

http.listen(constantVariable.SERVER_PORT, function () {
    logger.info('Server run on *:' + constantVariable.SERVER_PORT);
});