/**
 * Created by phuchoangmai on 29/09/2016.
 */
var io;
var gameSocket;

var config = require("../config/ConfigLog4JS"); //module Log4JS
var constantVariable = require("../sharevariables/ConstantVariable"); //module constant variable
var logger = config.getLogger("GameLogic.js"); //Get logger for this file
var responseBody = require("../bean/ResponseBody");
var questionGame = require("./QuestionGame");

var initGame = function (sio, socket, deviceId) {
    io = sio;
    gameSocket = socket;

    gameSocket.on(constantVariable.ACTION_GAME, actionGame);
}

module.exports = {
    initGame: initGame
}

/**
 * Two function emit(send) data to client
 * @param lane
 * @param data
 */
function emitData(lane, data) {
    io.emit(lane, data);
}

function emitInRoom(room, lane, data) {
    io.in(room).emit(lane, data);
}

/**
 * Function main action game
 */
function actionGame(dataRequest) {
    dataRequest = JSON.parse(dataRequest);
    if (!dataRequest['option'] || !dataRequest['userId']) {
        gameSocket.emit(constantVariable.ALL_MESSAGE,
            responseBody.mediumResponse('F', 'Bad request', constantVariable.BAD_REQUEST, null));

        return;
    }
    var requestOption = dataRequest.option;

    if (requestOption == constantVariable.CREATE_GAME) { //Action create game
        createNewGame(dataRequest);
        return;
    }
    if (requestOption == constantVariable.JOIN_GAME) { //Action join game
        joinGame(dataRequest);
        return;
    }
    if (requestOption == constantVariable.CHECK_ANSWER_GAME) { //Action check answer game
        checkAnswerGame(dataRequest);
        return;
    }
    if (requestOption == constantVariable.NEXT_ROUND) { //Action check answer game
        sendQuestion(dataRequest);
        return;
    }
    gameSocket.emit(constantVariable.ALL_MESSAGE,
        responseBody.mediumResponse('F', 'Bad request', constantVariable.BAD_REQUEST, null));

    return;
}
/**
 * Function a client create new game
 * @param dataRequest {fromDevice, toDevice}
 */
function createNewGame(dataRequest) {
    if (!dataRequest['friendId']) {
        emitData(dataRequest.userId,
            responseBody.mediumResponse('F', 'Bad request', constantVariable.BAD_REQUEST, null));

        return;
    }
    var gameId = dataRequest.userId + "-" + dataRequest.friendId + "-" + new Date().getTime();
    logger.info('[' + dataRequest.userId + '] created gameId: ' + gameId);

    //Send gameId to client create game
    emitData(dataRequest.userId,
        responseBody.mediumResponse('T', 'Create game success. Watting your friend join',
            constantVariable.CREATE_GAME, {
                'gameId': gameId
            }));

    //Send gameId to client invited game
    emitData(dataRequest.friendId,
        responseBody.mediumResponse('T', '[' + dataRequest.userId + '] invitation play game',
            constantVariable.JOIN_GAME, {
                'gameId': gameId
            }));

    //Client create join to room(gameId) client invited
    gameSocket.join(gameId);
}

/**
 * Client join game
 * @param dataRequest {gameId}
 */
function joinGame(dataRequest) {
    if (!dataRequest['gameId']) {
        emitData(dataRequest.userId,
            responseBody.mediumResponse('F', 'Bad request', constantVariable.BAD_REQUEST, null));

        return;
    }
    gameSocket.join(dataRequest.gameId);
    emitData(dataRequest.userId,
        responseBody.mediumResponse('T', 'Join game ['+dataRequest.gameId+"] success",
            constantVariable.NEW_MESSAGE, dataRequest.gameId));

    emitInRoom(dataRequest.gameId, constantVariable.GAME_MESSAGE,
        responseBody.gameConfigResponse('T',
            'Game ' + dataRequest.gameId + ' starting after ' + constantVariable.TIME_DOWN_START_GAME,
            constantVariable.STARTING_GAME, dataRequest.gameId, null));
}

/**
 * Client send request and server check answer of question
 * @param round
 * @param dataRequest
 */
function checkAnswerGame(dataRequest){
    if (!dataRequest['gameId']) {
        emitData(dataRequest.userId,
            responseBody.mediumResponse('F', 'Bad request', constantVariable.BAD_REQUEST, null));
        return;
    }
    var dataResponse = {
        'currentRound': dataRequest.currentRound,
        'totalRound' : dataRequest.totalRound
    };

    if(dataRequest.trueAnswer == dataRequest.clientAnswer){ //Check answer
        dataResponse.clientCorrectAnswer = dataRequest.userId;
        emitInRoom(dataRequest.gameId, constantVariable.GAME_MESSAGE,
            responseBody.gameConfigResponse('T','Round [' + round + "] client "+dataRequest.userId+" is correct answer",
                constantVariable.QUESTION_GAME, dataRequest.gameId, dataResponse));
    }

    //Check round in the last round
    if(dataRequest.currentRound = dataRequest.totalRound){
        emitInRoom(dataRequest.gameId, constantVariable.GAME_MESSAGE,constantVariable.GAME_OVER,
                constantVariable.GAME_OVER, dataRequest.gameId, null);
    }
    /*else{
        sendQuestion(dataRequest.currentRound + 1, dataRequest);
    }*/
}

/**
 * Server send new question to room
 * @param round
 * @param dataRequest
 */
var sendQuestion = function (dataRequest) {
    if(!dataRequest['level']){
        dataRequest.level = 1;
    }
    var dataResponse = questionGame.gennerateQuestionRandom(dataRequest.level);
    dataResponse.currentRound = parseInt(dataRequest.currentRound);
    dataResponse.totalRound = dataRequest.totalRound;
    emitInRoom(dataRequest.gameId, constantVariable.GAME_MESSAGE,
        responseBody.gameConfigResponse('T','This is question in round' + currentRound,
            constantVariable.QUESTION_GAME, dataRequest.gameId, dataResponse));
}