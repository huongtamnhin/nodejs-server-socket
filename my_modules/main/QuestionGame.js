/**
 * Created by phucnguyen on 03/02/2017.
 */
var config = require("../config/ConfigLog4JS"); //module Log4JS
var logger = config.getLogger("QuestionGame.js"); //Get logger for this file

var gennerateQuestionRandom = function(level){
    var dataResponse = {};
    var minQ = Math.pow(10,level-1);
    var maxQ = Math.pow(10,level) - 1;

    //Random question
    var question1 = randomInNumber(minQ, maxQ);
    var question2 = randomInNumber(minQ, maxQ);
    var question3 = 0;
    var operation = null;
    while (question1 == question2){
        question2 = randomInNumber(minQ, maxQ);
    }

    //Random operation
    var typeOpe = randomInNumber(1,4);
    switch (typeOpe){
        case 1: // operation is +
            question3 = question1 + question2;
            operation = "+";
            break;

        case 2: // operation is -
            question3 = question1 - question2;
            operation = "-";
            break;

        case 3: // operation is *
            question3 = question1 * question2;
            operation = "x";
            break;

        case 4: // operation is /
            question3 = question1;
            question1 = question1 * question2;
            operation = ":";
            break;
    }

    var arr = [question1,question2,question3];

    //Random position
    var position = randomInNumber(0,2);
    var answerT = arr[position];
    arr.splice(position, 1, "?");
    dataResponse.basicQuestion = arr[0] + " " + operation + " " + arr[1] + " = " + arr[2];
    dataResponse.trueAnswer = answerT;

    //Random Answer
    arr = [];
    arr.push(answerT+2);
    arr.push(answerT-3);
    arr.push(answerT-1);
    arr.push(answerT+3);

    //Random poisition true answer
    position = randomInNumber(0,3);
    arr.splice(position, 1, answerT);
    dataResponse.answer1 = arr[0];
    dataResponse.answer2 = arr[1];
    dataResponse.answer3 = arr[2];
    dataResponse.answer4 = arr[3];

    logger.info("Gennerate question ==> "+JSON.stringify(dataResponse));
    return dataResponse;
}

var randomInNumber = function(fromNumber, toNumber){
    return Math.floor(Math.random() * toNumber) + fromNumber;
}

module.exports = {
    gennerateQuestionRandom: gennerateQuestionRandom
}